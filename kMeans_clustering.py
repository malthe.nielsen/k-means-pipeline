import numpy as np
from matplotlib import pyplot as plt
from sklearn.cluster import KMeans 
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import silhouette_samples, silhouette_score
from sklearn.mixture import GaussianMixture
import sys
import glob

#  ISO_data = np.genfromtxt('resampled_iso_data_one_out.dat', delimiter = ',')
#  KD_data = np.genfromtxt('resampled_kd_data_one_out.dat', delimiter = ',')
#  PCA_data = np.genfromtxt('PCA_tranform_KD_ISO.dat', delimiter = ' ')

data = np.genfromtxt('PCA_Original.dat', delimiter = ' ')
data = data[:,:]

Mdir = 1000
mdir_list = np.arange(500,5500,500)
i = 0
avg_tpr = [] 
avg_tnr = []
avg_ssc = []
std_tpr = [] 
std_tnr = []
std_ssc = []
KD5  = [6, 8, 4, 7, 11]
ISO5 = [8, 6, 4, 7, 11]
both = [4, 6, 8]
for i in range(10):
    Mdir = mdir_list[i]
    print(i, 'i')
    dirs = glob.glob(f'{Mdir}/*.dat')
    SSco = []
    TPR_ = []
    TNR_ = []
    for udir in dirs:
        data_fram = np.genfromtxt(udir, delimiter = ' ')
        #  scaler = MinMaxScaler()
        #  scaler.fit(data_fram)
        data_fram = data_fram[:,:]
        ones = np.zeros((Mdir,1))
        zero = np.ones((Mdir,1))
        twos = np.ones((500, 1))*2
        tag  = np.concatenate((ones, zero))
        tag  = np.concatenate((tag, twos))
        data_fram = np.concatenate((data_fram, tag), axis = 1)
        np.random.shuffle(data_fram)
        data_test = data_fram[Mdir:, :-1]
        tag_test  = data_fram[Mdir:, -1]
        data_fram = data_fram[:Mdir, :-1]

        kmeans = KMeans(n_clusters=2, random_state=np.random.randint(1)).fit(data_fram)

        label = kmeans.labels_

        npred   = kmeans.predict(data_test)
        predict = kmeans.predict(data)
        print(predict)

        silhouette_avg = silhouette_score(data_fram, label)
        #  print("For n_clusters =", 2, "The average silhouette_score is :", silhouette_avg)
        SSco.append(silhouette_avg)

        tp = 0; tn = 0; fn = 0; fp=0;
        for i in range(len(tag_test)):
            if tag_test[i] == 1   and npred[i] == 1:
                tp += 1
            elif tag_test[i] == 0 and npred[i] == 0:
                tn += 1
            elif tag_test[i] == 1 and npred[i] == 0:
                fp += 1
            elif tag_test[i] == 0 and npred[i] == 1:
                fn += 1
        if tp > fp:
            TPR = tp /(tp+fn)
            TNR = tn /(tn+fp)
        elif tp < fp:
            TPR = (fp) /(fp+tn)
            TNR = (fn) /(fn+tp)
        TPR_.append(TPR)
        TNR_.append(TNR)
    avg_tnr.append(np.mean(TNR_))
    avg_tpr.append(np.mean(TPR_))
    avg_ssc.append(np.mean(SSco))
    std_tnr.append(np.std(TNR_))
    std_tpr.append(np.std(TPR_))
    std_ssc.append(np.std(SSco))


KD5  = [6, 8, 4, 7, 11]
ISO5 = [8, 6, 4, 7, 11]
both = [4, 6, 8]
print(avg_ssc)
print('////////////////////////////')
print(avg_tpr)
print('----------------------------')
print(avg_tnr)
save_file = np.asarray([avg_tpr, avg_tnr, avg_ssc, std_tpr, std_tnr, std_ssc])
np.savetxt('noise_PCA_all.txt', save_file, delimiter = ',')

