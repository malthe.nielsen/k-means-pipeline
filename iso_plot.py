import numpy as np
from matplotlib import pyplot as plt
import matplotlib as mpl
import sys
mpl.rcParams['xtick.labelsize'] = 16
mpl.rcParams['ytick.labelsize'] = 16
mpl.rcParams['axes.labelsize']  = 20
mpl.rcParams['axes.titlesize'] = 22
mpl.rcParams['legend.fontsize']  = 16

#cbar variabel:
pca_1 = np.genfromtxt('noise_PCA_1.txt', delimiter = ',')
pca_2 = np.genfromtxt('noise_PCA_2.txt', delimiter = ',')
pca_3 = np.genfromtxt('noise_PCA_3.txt', delimiter = ',')
pca_a = np.genfromtxt('noise_PCA_all.txt', delimiter = ',')

noi_a = np.genfromtxt('noise_all.txt', delimiter = ',')
noi_v = np.genfromtxt('noise_vol.txt', delimiter = ',')
noi_f = np.genfromtxt('noise_fys.txt', delimiter = ',')

for_1 = np.genfromtxt('forrest_1.txt', delimiter = ',')
for_2 = np.genfromtxt('forrest_2.txt', delimiter = ',')
for_3 = np.genfromtxt('forrest_3.txt', delimiter = ',')
for_a = np.genfromtxt('forrest_all.txt', delimiter = ',')


Ns = np.array( [500 ,1000, 1500,2000, 2500, 3000, 3500, 4000, 4500, 5000 ])

#  colourbar shit:
norm = mpl.colors.Normalize( vmin = Ns.min(), vmax = Ns.max() )
cmap = mpl.cm.ScalarMappable( norm = norm, cmap = mpl.cm.get_cmap('Blues') )
cmap.set_array([])
fig = plt.figure( figsize = (10,8) )

#det vigtiger bare at c = c,ap.to_rgba(Ns)


plt.scatter(for_1[0, :], for_1[1, :],  c=cmap.to_rgba(Ns), zorder = 30,marker = 'o')
plt.errorbar(for_1[0, :], for_1[1, :],xerr=for_1[-3,:], yerr=for_1[-2,:],  fmt = '.',c='k')

cbar = plt.colorbar(cmap)
cbar.set_label('N ISO FOREST + PCA')
cbar.ax.tick_params(labelsize = 16)

Ns = np.array( [100,500 ,1000, 1500,2000, 2500, 3000, 3500, 4000, 4500, 5000 ])
cmap = mpl.cm.ScalarMappable( norm = norm, cmap = mpl.cm.get_cmap('Reds') )
cmap.set_array([])
plt.scatter(noi_a[0, :], noi_a[1, :],  c=cmap.to_rgba(Ns), zorder = 30,marker = 'o')
plt.errorbar(noi_a[0, :], noi_a[1, :],xerr=noi_a[-3,:], yerr=noi_a[-2,:],  fmt = '.',c='k')

cbar = plt.colorbar(cmap)
cbar.set_label('N PCA ONLY')
cbar.ax.tick_params(labelsize = 16)

plt.ylabel('Specificity')
plt.xlabel('Sensitivity')
plt.grid()
plt.tight_layout()
#  plt.savefig('pca_vs_for')
plt.show()

N_point = np.arange(500,5500, 500)

fig, plot = plt.subplots(1,1, figsize = (8,8))
plot.errorbar(N_point, noi_a[2, 1:], yerr = noi_a[-1,1:], label = 'All data', marker = 'o',ls = 'none')
plot.errorbar(N_point, pca_1[2, :], yerr = pca_1[-1,:], label = 'PC1', marker = 'o', ls = 'none')
plot.errorbar(N_point, pca_2[2, :], yerr = pca_2[-1,:], label = 'PC2', marker = 'o', ls = 'none')
plot.errorbar(N_point, pca_3[2, :], yerr = pca_3[-1,:], label = 'PC3', marker = 'o', ls = 'none')
plot.errorbar(N_point, pca_a[2, :], yerr = pca_a[-1,:], label = 'PC all', marker = 'o', ls = 'none')

#  plot.errorbar(N_point, for_a[2, :], yerr = for_a[-1,:], label = 'Forrest all', marker = 'o', ls = 'none')
#  plot.errorbar(N_point, for_1[2, :], yerr = for_1[-1,:], label = 'Forrest PC1', marker = 'o', ls = 'none')
#  plot.errorbar(N_point, for_2[2, :], yerr = for_2[-1,:], label = 'Forrest PC2', marker = 'o', ls = 'none')
#  plot.errorbar(N_point, for_3[2, :], yerr = for_3[-1,:], label = 'Forrest PC3', marker = 'o', ls = 'none')
#  plot.errorbar(N_point, noi_v[2, :], yerr = noi_v[-1,:], label = 'Volumetric data', marker = 'o', ls = 'none')
#  plot.errorbar(N_point, noi_f[2, :], yerr = noi_f[-1,:], label = 'Physical data', marker = 'o', ls = 'none')
#  plot.scatter(N_point, noi_b[2, :], label = 'Linear combi')
#  plot.scatter(N_point, noi_i[2, :], label = 'Best 5')
plot.set_title('Silhouette score')
plot.set_xlabel('Sample size')
plot.set_ylabel('Score')
plot.legend(loc = 8, ncol = 3)
plot.set_ylim(0, 0.8)
plt.tight_layout()
fig.savefig('PCA_only')
plt.show()
#
#  plt.errorbar(N_point, pca_1[1, :], yerr = pca_1[-2,:], label = 'PC1')
#  plt.scatter(N_point, pca_2[1, :], label = 'PC2')
#  plt.scatter(N_point, pca_3[1, :], label = 'PC3')
#  plt.scatter(N_point, pca_a[1, :], label = 'PC all')
#
fig, plot = plt.subplots(1,1, figsize = (8,8))
plot.errorbar(N_point, noi_a[2, 1:], yerr = noi_a[-1, 1:], label = 'All data', marker = 'o', ls = 'none')
plot.errorbar(N_point, noi_v[2, 1:], yerr = noi_v[-1, 1:], label = 'Volumetric data', marker = 'o', ls = 'none')
plot.errorbar(N_point, noi_f[2, 1:], yerr = noi_f[-1, 1:], label = 'Physical data', marker = 'o', ls = 'none')
plot.set_title('Silhouette score')
plot.set_xlabel('Sample size')
plot.set_ylabel('Score')
plot.legend(loc = 8, ncol = 2)
plot.set_ylim(0.3, 0.55)
plt.tight_layout()
fig.savefig('only_cluster')
#  plt.scatter(N_point, noi_b[1, :], label = 'Linear combi')
#  plt.scatter(N_point, noi_i[1, :], label = 'Best 5')
#  plt.title('Specificity score')
#  plt.legend()
plt.show()
#
#  plt.errorbar(N_point, pca_1[0, :], yerr = pca_1[-3,:], label = 'PC1')
#  plt.scatter(N_point, pca_2[0, :], label = 'PC2')
#  plt.scatter(N_point, pca_3[0, :], label = 'PC3')
#  plt.scatter(N_point, pca_a[0, :], label = 'PC all')
#
plt.errorbar(N_point, noi_a[0, :], yerr = noi_a[-3,:], label = 'All data')
plt.errorbar(N_point, noi_v[0, :], yerr = noi_v[-3,:], label = 'Volumetric data')
plt.errorbar(N_point, noi_f[0, :], yerr = noi_f[-3,:], label = 'Physical data')
#  plt.scatter(N_point, noi_b[0, :], label = 'Linear combi')
#  plt.scatter(N_point, noi_i[0, :], label = 'Best 5')
#  plt.title('Sensitivity score')
#  plt.legend()
plt.show()
