import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats
import matplotlib as mpl

mpl.rcParams['xtick.labelsize'] = 16
mpl.rcParams['ytick.labelsize'] = 16
mpl.rcParams['axes.labelsize']  = 18
mpl.rcParams['axes.titlesize'] = 18
mpl.rcParams['legend.fontsize']  = 16

data_dir = r'C:\Users\Carl\Documents\AMAS\group_assignment\data'
#data_control = np.loadtxt(data_dir+'\control_improved.txt', skiprows=1 , delimiter = ',')
data_iso = np.loadtxt(data_dir+'\ISO_ext.txt', skiprows=1 , delimiter = ',')
data_kd = np.loadtxt(data_dir+'\KD_ext.txt', skiprows=1 , delimiter = ',')
#data_lps = np.loadtxt(data_dir+'\LPS.txt', skiprows=1 , delimiter = ',')
#print(np.shape(data_control))
print(np.shape(data_iso))
print(np.shape(data_kd))
#print(np.shape(data_lps))

#500x2 +500 data point test generation:

def resampler(data, cov, N):

    virt_data = np.zeros (len(data[0,:]))
    print(np.shape(virt_data))

    choices = []
    #n = 0
    for i in range (N):
        choice = np.random.choice(list(range(len(data[:,0]))))
        choices.append(choice)
    #use whole row.
        coord, varis = [data[choice,:] ,cov]
        made_data = np.random.multivariate_normal( coord, varis, size = 1)
    #print(made_data)
    #print(made_data)
    #print(made_data)
        #n  = + 1
        virt_data = np.vstack((virt_data, made_data[0]))
        #print(np.shape(virt_data))
    return virt_data[1:,:]




cov_mtx = np.zeros((12,12))
np.fill_diagonal(cov_mtx, [75**2,25,1**2,100,(500)**2,120**2,1000**2,250000,(1000)**2,(100)**2,(100)**2,(500)**2])
print(len([75**2,25,1,100,(500)**2,100**2,1000**2,250000,(1000)**2,(1000)**2,(1000)**2,(1)**2]))
#print(np.shape(data_control))


#r_control = resampler(data_control, cov_mtx, 500)
r_iso = resampler(data_iso, cov_mtx, 500)
r_kd = resampler(data_kd, cov_mtx, 500)
#r_lps = resampler(data_lps, cov_mtx, 500)





#generating data:


for i in range(1,11):
    #print(i)
    N_trials = 10
    for k in range(N_trials):
        r_iso = resampler(data_iso, cov_mtx, int(i)*500)
        r_kd = resampler(data_kd, cov_mtx, int(i)*500)
        X = np.vstack((r_iso, r_kd))
    #print(noise_tester(500))
        #n100=np.array([0,0])
   
        sys_err_data = (np.random.uniform( size = (500,12) )*err_scale.T)+err_loc.T
        data_c = np.vstack((X,sys_err_data))
        np.savetxt(fr"C:\\Users\\Carl\\Documents\\AMAS\\group_assignment\\varied_N_data\\{i*500}\\N_{int(i*500)}_nr{k}.dat",data_c )
		
		
